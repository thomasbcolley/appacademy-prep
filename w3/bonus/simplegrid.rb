class SimpleGrid
  attr_reader :width, :height
  def initialize(width, height)
    validate_dimensions(width, height)

    @width, @height = width, height
    @big_dim = @width > @height ? :width : :height
    @values = Array.new(@width * @height)

  end

  def validate_dimensions(width, height)
      if not (height.is_a?(Integer) && width.is_a?(Integer) && height > 0 && width > 0)
        raise "Invalid Dimensions for Grid #{height}x#{width}"
      end
      true
  end

  def height=(args) #Do not use.
    raise "Do not change height."
  end

  def width=(args) #Do not use.
    raise "Do not change width."
  end

  # key(x, y) Creates a one-to-one mapping from (x, y) onto (0...x*y)
  # If @height or @width change after initialize, key(x, y) will break.
  # Do not alter any key_ function without changing all of them to match.
  def key(x, y)
    if @big_dim == :width
      @width * y + x
    elsif @big_dim == :height
      @height * x + y
    else
      raise "@big_dim (#{@big_dim.inspect}) is not correctly set."
    end
  end

  def key_coords(key)
    [key_x(key), key_y(key)]
  end

  def key_x(key)
    #See proofs in key_y.
    if @big_dim == :width
      key / @height
    elsif @big_dim == :height
      key % @width
    else
      raise "@big_dim is not set unknown."
    end
  end

  def key_y(key)
    if @big_dim == :width
      key / @width
      #PROOF
        #key = width * y + x
        #  => y = (key - x)/width = key / width + x / width
        #  && x < width => x / width = 0 [integer division]
        #  => y = key / width
        # NOTE: due to width and height requirements, width != 0
    elsif @big_dim == :height
      key % @height
      #PROOF
        #key = height * x + y
        #  => key % height = (height * x + y) % height = y
        #  => key % height = y
        #  => y = key % height
        # NOTE: due to width and height requirements, height != 0
    else
      raise "@big_dim is not set unknown."
    end
  end

  def valid_coordinates?(x, y)
    valid = true
    valid = valid && (x.is_a?(Integer) && y.is_a?(Integer))
    valid = valid && (x >= 0 && y >= 0)
    valid = valid && (x < @width && y < @height)
  end

  def count
    @values.count
  end

  def to_s #This method really isn't for general use. It exists primarily for Maze
    str = ""
    0.upto(@height.pred) do |y|
      0.upto(@width.pred) do |x|
        obj = get(x, y)
        str << obj.to_s
      end
      str << "\n"
    end
    str.chomp
  end

  #returns [x, y] from key
  #key_ functions are all interdependent and rely on @width, @height.
  #do not change one without changing all.

  #Consider renaming to set and get vs assign and retrieve
  def set(x, y, value)
    raise "Invalid Coordinate (#{x}, #{y}) for width: #{@width} ; height: #{@height}" unless valid_coordinates?(x, y)
    @values[key(x, y)] = value
  end

  #Returns suppress_exception if (invalid coords and suppress_exception is truthy).
  def get(x, y, suppress_exception=false)
    unless valid_coordinates?(x, y)
      if suppress_exception
        return suppress_exception
      else
        raise "Invalid Coordinates (#{x}, #{y})."
      end
    end
    @values[key(x, y)]
  end

  def neighbor_coordinates(x, y, direction)
    #$allowed_directions should not include anything not found here.
    case direction
    when :uu
      y -= 1
    when :dd
      y += 1
    when :ll
      x -= 1
    when :rr
      x += 1
    when :ul
      y -= 1
      x -= 1
    when :ur
      y -= 1
      x += 1
    when :dl
      y += 1
      x -= 1
    when :dr
      y += 1
      x += 1
    else
      raise "Invalid Direction (#{direction.inspect})."
    end
    [x, y]
  end

  def neighbor(x, y, direction, return_when_invalid=:invalid_neighbor)
    return return_when_invalid unless valid_coordinates?(x, y)

    coords = neighbor_coordinates(x, y, direction)
    x = coords[0]
    y = coords[1]

    get(x, y, return_when_invalid)
  end

  #Refactor Candidate: don't use select. seems costly.
  def neighbors(x, y)
      $allowed_directions.map do |direction|
        neighbor(x, y, direction, :invalid_neighbor)
    end.select {|val| val != :invalid_neighbor}
  end

  #Calculates distance between two points assuming you can travel
  # => left, right, up, down, or any square diagonal.
  def SimpleGrid.default_distance(x1, y1, x2, y2)
    width = (x2 - x1).abs
    height = (y2 - y1).abs
    diagonal_travel = [width, height].min
    regular_travel = [width, height].max - diagonal_travel
    (regular_travel)*REGULAR_COST + (diagonal_travel)*DIAGONAL_COST
  end

  def SimpleGrid.distance(x1, y1, x2, y2, &prc)
    if not block_given?
      SimpleGrid.default_distance(x1, y1, x2, y2)
    else
      prc.call(x1, y1, x2, y2)
    end
  end

  def each_neighbor_with_coordinates(x, y, &prc)
      $allowed_directions.each do |direction|
        neigh = neighbor(x, y, direction, :invalid_neighbor)
        prc.call(x, y, neigh)
    end
  end

  def each_coordinate_pair(&prc)
    0.upto(@height.pred) do |y|
      0.upto(@width.pred) do |x|
        # print "HEIGHT: #{@height} ; WIDTH: #{@width} ; ..."
        prc.call(x, y)
      end
    end
  end

  def each_with_coordinates(&prc)
    each_coordinate_pair do |x, y|
      element = get(x, y)
      prc.call(x, y, element)
    end
  end

#Refactor: once we have a #dup method, use map!
  def map(&prc)
    result = SimpleGrid.new(@width, @height)
    each_coordinate_pair do |x, y|
      cell = get(x, y)
      result.set(x, y, prc.call(cell))
    end
    result
  end

  def map!(&prc)
      each_coordinate_pair do |x, y|
        cell = get(x, y)
        set(x, y, prc.call(cell))
      end
      self
  end

  def map_with_coordinates(&prc)
    result = SimpleGrid.new(@width, @height)
    each_coordinate_pair do |x, y|
      cell = get(x, y)
      result.set(x, y, prc.call(x, y, cell))
    end
    result
  end
end
