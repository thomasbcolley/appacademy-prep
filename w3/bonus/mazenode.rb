class MazeNode
  attr_reader :x, :y, :type
  def initialize(x, y, type)
    @@charmap ||= {start: "S", end: "E", wall: "*", space: " ", path: "X", DEFAULT: "+"}
    @type = type
    @x = x
    @y = y
  end

  def start?
    @type == :start
  end

  def end?
    @type == :end
  end

  def wall?
    @type == :wall
  end

  def space?
    @type == :space
  end

  def path?
    @type == :path
  end

  def path!
    @type = :path unless end? || start?
  end

  def impassable?
    wall?
  end

  def passable?
    not impassable?
  end

  def to_s
    @@charmap[type] || @@charmap[:DEFAULT]
  end
end
