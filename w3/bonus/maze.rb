$allowed_directions = [:ll, :rr, :uu, :dd, :ul, :ur, :dl, :dr] #diagonals included
$diags_allowed = true
#$allowed_directions = [:ll, :rr, :uu, :dd] #diagonals excluded
DIAGONAL_COST = 14
REGULAR_COST = 10

require_relative "simplegrid"
require_relative "mazenode"
require_relative "astar"

=begin TODO
  Create a global variable for char_map
  Create a global variable for reverse_char_map
  Alternatively, create a method that takes one and converts it to the other.
  Clean up some methods.
=end
=begin
The top left of the maze is point (0,0)
Traveling down increases y coordinate
Traveling right increases x coordinate
=end


class Maze
  def initialize(width, height)
    @grid = SimpleGrid.new(width, height)
    @start_node = nil
    @end_node = nil
  end

  def Maze.parse_string(maze_string)
    char_map = {"S" => :start, "E" => :end, " " => :space, "*" => :wall, "X" => :path} #Really, this should be a constant. DRY

    maze_string.strip!
    height = maze_string.count("\n") + 1
    width = (maze_string =~ /\n/) || maze_string.length
    parsed_maze = Maze.new(width, height)

    x, y = 0, 0
    maze_string.chars.each do |char|
      if char == "\n"
        x = 0; y += 1
        next
      end
      raise "Invalid character received (#{char})." unless char_map.has_key?(char)

      node_type = char_map[char]
      node = MazeNode.new(x, y, node_type)
      parsed_maze.add_node(node)

      x += 1
    end
    parsed_maze
  end

  def Maze.get_maze_string_from_user
    key="\"S\"=Start, \"*\"=Wall, \"E\"=End, \" \"=Space. \"/\" => delete previous line \"&\" => end of maze"
    user_input = ""
    maze_data = ""
    puts "Build a maze! Hit enter on an & to conclude maze.\nNo error checking will be performed, so make sure your maze make sense!"
    until user_input == "&\n"
      puts "\n\nKEY: #{key}\n\nMaze so far:\n\n"
      maze_data.split("\n").each {|line| puts ": #{line}<EOL>"}
      print ": "
      user_input = gets
      case user_input
      when "&\n"
        break
      when /\/+\n/
        maze_data = maze_data.split("\n")
        maze_data.pop(user_input.count("/"))
        maze_data = maze_data.join("\n") + "\n"
      else
        maze_data << user_input
      end
    end
    maze_data
  end

  def a_star_solve!
    unless @start_node && @end_node
      puts "No Solution: Missing Start or End."
      return self
    end

    a_star_grid = @grid.map_with_coordinates do |x, y, node|
      AStarNode.new(x, y, node.passable?)
    end

    a_star_start = a_star_grid.get(@start_node.x, @start_node.y)
    a_star_target = a_star_grid.get(@end_node.x, @end_node.y)
    a_star = AStar.new(a_star_grid, a_star_start, a_star_target)

    solution_grid = a_star.solve!
    merge_path(solution_grid)
    self #self is unaffected if no start or end
  end

  def merge_path(solution_grid) #Merges grid of true/false values with
    solution_grid.each_with_coordinates do |x, y, is_path|
      if is_path
        @grid.get(x, y).path!
      end
    end
  end

  def add_node(node)
    @start_node = node if node.start?
    @end_node = node if node.end?
    x, y = node.x, node.y

    @grid.set(x, y, node)
  end

  def to_s
    @grid.to_s
  end
end


if __FILE__ == $PROGRAM_NAME
=begin
The following is for demo purposes. The code is ugly, but oh well.
=end
  user_input = ""
  $diags_allowed = true
  while 1
    maze_data = ""
    print "\n\nPlease enter a command (exit, file, user, diags): "
    user_input = gets.chomp
    case user_input
    when "exit"
      break
    when "file"
      print "\n\nEnter Filename:  ./mazes/"
      user_input = gets.chomp
      file_name = "./mazes/#{user_input}"
      maze_data = File.read(file_name)
    when "user"
      maze_data = Maze.get_maze_string_from_user
    when "diags"
      $diags_allowed = $diags_allowed ^ true
      $allowed_directions = [:ll, :rr, :uu, :dd, :ul, :ur, :dl, :dr] if $diags_allowed
      $allowed_directions = [:ll, :rr, :uu, :dd] if not $diags_allowed
      print "Diagonals are now #{$diags_allowed ? "allowed." : "excluded."}\n"
      next
    else
      print "invalid input.\n"
      next
    end

    maze_data = Maze.parse_string(maze_data)
    print "\e[2J"
    puts "UNSOLVED MAZE:\n\n#{maze_data}\nPress enter to get solution."
    gets
    maze_data.a_star_solve!
    puts "SOLVED MAZE:\n\n#{maze_data}"
  end
end
