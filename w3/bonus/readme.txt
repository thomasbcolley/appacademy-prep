For a demonstration, run
  ruby maze.rb

  There are four main options. The options are pretty self explanatory.
  File prompts you to load a file.
  user prompts you to create a maze by hand.
  exit exits.
  diags toggles diagonal travel.
