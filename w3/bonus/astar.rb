class AStarNode #This class exists strictly to encapsulate data.
  attr_reader :x, :y, :data
  attr_accessor :g, :h, :parent
  def initialize(x, y, data)
    @x = x
    @y = y
    @g = 0
    @h = 0
    @closed = false
    @parent = nil
    @data = data
  end

  def closed!
    @closed = true
  end

  def passable?
    @data == true
  end

  def impassable?
    not passable?
  end

  def closed?
    @closed
  end

  def backtrack!
    @data = :path
    @parent.backtrack! unless parent.nil?
  end

  def f
    g + h
  end
end

# AStar.new(grid, start_node, target_node)
# => expects grid of valid AStarNodes
# => each AStarNode.data should be either true or false
# => true indicates passable
# => false indicates impassable
# => expects start_node to be a node within the passed grid
# => expects target_node to be a node within the passed grid

class AStar
  def initialize(grid, start_node, target_node)
    @grid = grid
    @start_node = start_node
    @target_node = target_node
    @start_node.g = 0
    @open = []
    @closed = []
  end

  def solve!
    target_reached = false
    @open = [@start_node]
    iterations = 0
    until @open.empty? || target_reached
      iterations += 1

      parent = best_candidate
      parent.closed!

      successors = @grid.neighbors(parent.x, parent.y)
      successors.each do |successor|
        next unless successor.passable?
        update_successor_f(successor, parent)

        if successor == @target_node
          target_reached = true
        end#if
      end#do
    end#until

    @target_node.backtrack! if target_reached
    puts "Iterations to solve: #{iterations}."
    @grid.map! {|node| node.data == :path ? true : false}
  end#solve

  def update_successor_f(successor, parent)
    successor_new_g = parent.g + SimpleGrid.distance(parent.x, parent.y, successor.x, successor.y)
    successor_h = SimpleGrid.distance(successor.x, successor.y, @target_node.x, @target_node.y)

    successor_is_open_or_closed = successor.closed? || @open.include?(successor)
    #We want to update open and closed successors, but only if we have a better g.
    if successor_is_open_or_closed && successor_new_g >= successor.g#This whole bit of flow control could be put into its own method.
      return false
    else
      successor.parent = parent
      successor.g = successor_new_g
      successor.h = successor_h
      @open.push(successor) unless successor_is_open_or_closed
    end
  end

  def best_candidate
    minimum_f = @open.min_by {|node| node.f}.f #get's the element with lowest f, and the get's that element's f
    parent = @open.select {|node| node.f == minimum_f} # Get all open nodes with same f value.
    parent = @open.delete(parent.min_by {|node| node.h}) # Pick the node with the lowest h value. (Tie breaker) remove it from @open.
  end

end
