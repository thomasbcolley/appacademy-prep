#Status: complete.

def reverser
  sentence = yield
  sentence.split(" ").map { |word| word.reverse }.join(" ")
end

def adder(x = 1)
  yield + x
end

def repeater(times = 2)
  times.times { yield }
end
