# Status: complete.

def ftoc(degrees_fahrenheit)
  degrees_celsius = (degrees_fahrenheit - 32) * (5.0 / 9.0)
end

def ctof(degrees_celsius)
  degrees_fahrenheit = degrees_celsius * (9.0 / 5.0) + 32
end
