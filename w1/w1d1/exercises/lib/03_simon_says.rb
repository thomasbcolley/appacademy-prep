# Status: complete.

def echo(phrase)
  phrase
end

def shout(phrase)
  phrase.upcase
end

def repeat(phrase, times = 2)
  phrase + (" " + phrase) * times.pred
end

def start_of_word(phrase, length)
  phrase[0...length]
end

def first_word(phrase)
  phrase.split(" ").first
end

def titleize(phrase)
  do_not_capitalize = %w{and the over}
  # "the STRinG Of CHARACTERS".downcase.capitalize => "The string of characters"
  words = phrase.downcase.capitalize.split(" ")
  words.map! { |word| do_not_capitalize.include?(word) ? word : word.capitalize }
  words.join(" ")
end
