# Status: complete.

def translate(phrase)
  pig_latin_words = phrase.split(" ").map do |word|
    capital_word = (word[0] =~ /[A-Z]/)
    word = word.downcase
    if capital_word then pig_latin_word(word).capitalize else pig_latin_word(word) end
  end
  pig_latin_words.join(" ")
end


def pig_latin_word(word)
  vowels = %w(a e i o u)
  special_letters = {"q" => "qu"}

  split_index = 0
  while split_index < word.length
    current_letter = word[split_index]

    if special_letters.has_key?(current_letter)
      special_letter = special_letters[current_letter]

      if word[split_index..-1].start_with?(special_letter)
        split_index += special_letter.length
        next
      end
    end

    break if vowels.include?(word[split_index])
    split_index += 1
  end

  word[split_index..-1] + word[0...split_index] + "ay"
end
