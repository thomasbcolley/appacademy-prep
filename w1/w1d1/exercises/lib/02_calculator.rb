# Status: complete.

def add(x, y)
  x + y
end

def subtract(x, y)
  x - y
end

def sum(numbers)
  numbers.empty? ? 0 : numbers.reduce(:+)
end

def multiply(*numbers)
  numbers.reduce(:*)
end

def power(base, power)
  base**power
end

def factorial(big_factor)
  return 1 if big_factor == 0
  1.upto(big_factor).reduce(:*)
end
