# Status: complete.

# Sum
#
# Write an Array method, `sum`, that returns the sum of the elements in the
# array. You may assume that all of the elements are integers.

class Array
  def sum
    self.empty? ? 0 : reduce(:+)
  end
end

# Square
#
# Write an array method, `square`, that returns a new array containing the
# squares of each element.

class Array
  def square
    squared = self.dup
    squared.square!
  end
end

class Array
  def square!
    map! {|elem| elem * elem}
  end
end
# Finding Uniques
#
# Monkey-patch the Array class with your own `uniq` method, called
# `my_uniq`. The method should return the unique elements, in the order
# they first appeared:
#
# ```ruby
# [1, 2, 1, 3, 3].my_uniq # => [1, 2, 3]
# ```
#
# Do not use the built-in `uniq` method!

class Array
  def my_uniq
    self & self # This might be cheating.
    # uniqs = []; each {uniqs.include?(elem) || uniqs << elem}
  end
end

# Two Sum
#
# Write a new `Array#two_sum` method that finds all pairs of positions
# where the elements at those positions sum to zero.
#
# NB: ordering matters. I want each of the pairs to be sorted smaller
# index before bigger index. I want the array of pairs to be sorted
# "dictionary-wise":
#
# ```ruby
# [-1, 0, 2, -2, 1].two_sum # => [[0, 4], [2, 3]]
# ```
#
# * `[0, 2]` before `[1, 2]` (smaller first elements come first)
# * `[0, 1]` before `[0, 2]` (then smaller second elements come first)

class Array
  def two_sum
    pairs = Array.new(0)
    return pairs if count < 2
    0.upto(count - 2) do |first_index|
      first_index.next.upto(count - 1) do |second_index|
        pairs.push([first_index, second_index]) if self.at(first_index) + self.at(second_index) == 0
      end
    end
    pairs
  end
end

# Median
#
# Write a method that finds the median of a given array of integers. If
# the array has an odd number of integers, return the middle item from the
# sorted array. If the array has an even number of integers, return the
# average of the middle two items from the sorted array.

class Array
  def median
    return nil if self.empty?
    if self.length.even?
      (self.sort[length / 2] + self.sort[length / 2 - 1]) / 2.0
    else
      self.sort[length / 2]
    end
  end
end

# My Transpose
#
# To represent a *matrix*, or two-dimensional grid of numbers, we can
# write an array containing arrays which represent rows:
#
# ```ruby
# rows = [
#     [0, 1, 2],
#     [3, 4, 5],
#     [6, 7, 8]
#   ]
#
# row1 = rows[0]
# row2 = rows[1]
# row3 = rows[2]
# ```
#
# We could equivalently have stored the matrix as an array of
# columns:
#
# ```ruby
# cols = [
#     [0, 3, 6],
#     [1, 4, 7],
#     [2, 5, 8]
#   ]
# ```
#
# Write a method, `my_transpose`, which will convert between the
# row-oriented and column-oriented representations. You may assume square
# matrices for simplicity's sake. Usage will look like the following:
#
# ```ruby
# matrix = [
#   [0, 1, 2],
#   [3, 4, 5],
#   [6, 7, 8]
# ]
#
# matrix.my_transpose
#  # => [[0, 3, 6],
#  #    [1, 4, 7],
#  #    [2, 5, 8]]
# ```
#
# Don't use the built-in `transpose` method!

class Array
  def my_transpose
    size_of_first_sub_array = nil
    self.each do |sub_array|
      raise TypeError, "no implicit conversion of #{sub_array.class} into #{self.class}" unless sub_array.is_a?(self.class)

      size_of_first_sub_array ||= sub_array.size
      raise IndexError, "element size differs (#{sub_array.size} should be #{size_of_first_sub_array})" unless sub_array.size == size_of_first_sub_array
    end

    transposed = Array.new(size_of_first_sub_array) {Array.new(count)}
    self.each_index do |first_index|
      self[idx1].each_index do |second_index|
        transposed[second_index][first_index] = self[first_index][second_index]
      end
    end
    transposed
  end
end

# Bonus: Refactor your `Array#my_transpose` method to work with any rectangular
# matrix (not necessarily a square one).
