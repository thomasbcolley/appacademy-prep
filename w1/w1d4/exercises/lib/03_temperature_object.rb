# Status: complete.

class Temperature
  #Temperature is stored in @temperature_celsius and then convereted to other systems as needed.
  def Temperature.ftoc(fahrenheit)
    celsius = ((fahrenheit - 32.0) * 5.0) / 9.0
  end

  def Temperature.ctof(celsius)
    fahrenheit = (celsius * 9.0) / 5.0 + 32.0
  end

  #Takes options c: temp or f: temp. :c option is given precedence.
  def initialize(options)
    @temperature_celsius = options[:c] || Temperature.ftoc(options[:f])
  end

  #Returns temperature of object, in Fahrenheit.
  def in_fahrenheit
    Temperature.ctof(@temperature_celsius)
  end

  #Returns temperature of object, in Celsius.
  def in_celsius
    @temperature_celsius
  end

  #Returns a new Temperature object created from a Celsius temperature.
  def Temperature.from_celsius(celsius)
    Temperature.new(:c => celsius)
  end

  #Returns a new Temperature object created from a Fahrenheit temperature.
  def Temperature.from_fahrenheit(fahrenheit)
    Temperature.new(:f => fahrenheit)
  end

end

class Celsius < Temperature
  def initialize(degrees_celsius)
    @temperature_celsius = degrees_celsius
  end
end

class Fahrenheit < Temperature
  def initialize(degrees_fahrenheit)
    @temperature_celsius = Fahrenheit.ftoc(degrees_fahrenheit)
  end
end
