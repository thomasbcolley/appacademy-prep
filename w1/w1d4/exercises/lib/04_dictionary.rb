# Status: complete.

class Dictionary
  attr_reader :entries

  def initialize()
      @entries = {}
  end

  def add(option)
    if Hash === option
      option.each {|word, definition| entries[word] = definition}
    elsif String === option
      entries[option] = nil
    end
    @entries = Hash[@entries.sort_by {|word, definition| word.downcase}] # I suspect that this breaks in earlier versions of Ruby where Hash Key order is not necessarily preserved.
  end

  def include?(keyword)
    keywords.include?(keyword)
  end

  def keywords()
    @entries.keys
  end

  def find(prefix)
    @entries.select {|word, definition| word.start_with?(prefix)}
  end

  def printable
    printer = []
    @entries.each {|word, def| printer << "[#{word}] \"#{def}\""}
    printer.join("\n")
  end

end
