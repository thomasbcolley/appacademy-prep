# Status: complete.

SMALL_WORDS = %w(a an and the in of)

class Book
  attr_reader :title

  def title=(string)
    @title = titleize(string)
  end
private

  def titleize(phrase)
    words = phrase.downcase.capitalize.split(" ")#sanitizes phrase and caps first word
    words.map! { |word| SMALL_WORDS.include?(word) ? word : word.capitalize }
    words.join(" ")
  end

end
