# Status: complete.

class Timer
  attr_accessor :seconds
  def initialize
    @seconds = 0
  end

  def time_string
    "#{time_part(@seconds, :hour)}:#{time_part(@seconds, :minute)}:#{time_part(@seconds, :second)}"
    # Without time_part:
    # parts [seconds % 60, (seconds / 60) % 60, seconds / (60 * 60)]
    # "#{parts[2].to_s.rjust(2, "0")}:#{parts[1].to_s.rjust(2, "0")}:#{parts[0].to_s.rjust(2, "0")}"
  end

  def time_part(seconds, unit_of_time, pad_to=2)
    seconds_per_unit = {second: 1, minute: 60, hour: 3600, day: 86400}
    upper_bounds = {second: 60, minute: 60, hour: nil, day: nil}

    part = seconds / seconds_per_unit[unit_of_time]
    part %= (upper_bounds[unit_of_time]) unless upper_bounds[unit_of_time].nil?
    part.to_s.rjust(pad_to, "0")
  end
end
