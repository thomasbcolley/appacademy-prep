#Status: complete.
#Status: could probably be cleaned up a bit.
# => I made some fairly strange decisions in some places.

class RPNCalculator

  def initialize()
    @tokens = []
    @value = nil
    @valid_operators = [:+, :-, :*, :/]
  end

  # All modifications to @tokens should happen via push;
  # no other method should directly push to @tokens.
  def push(token)
    sanitary_token = sanitize(token)
    @tokens.push(sanitize(token))
    @value = evaluate_tokens(@tokens) #if sanitary_token.is_a? Symbol #This is a clumsy way to error check.
    self
  end

  def value
    @value
  end

  def minus
    push(:-)
  end

  def plus
    push(:+)
  end

  def times
    push(:*)
  end

  def divide
    push(:/)
  end

  def evaluate(phrase)
    evaluate_tokens(tokenize(phrase))
  end

  def tokens(phrase=nil)
    case phrase
    when String
      tokenize(phrase)
    when NilClass
      @tokens
    else
      raise ArgumentError, "invalid phrase: #{phrase.inspect}"
    end
  end

  private

  # I do not like how ArgumentError is handled here. It seems extreme to completely terminate the program. Look in to other ways of handling this.
  def sanitize(token)
    begin
      sanitary_token = Float token
    rescue TypeError, ArgumentError
      sanitary_token = token.to_sym
      raise ArgumentError, "Token (#{sanitary_token.inspect}), not found in @valid_operators (#{@valid_operators.inspect})." unless @valid_operators.include?(sanitary_token)
    end
    sanitary_token
  end

  #parses phrase into a sanitized array of tokens.
  def tokenize(phrase)
    phrase.split.map do |token_as_string|
      sanitize(token_as_string)
    end
  end

  def evaluate_tokens(tokens) #Doesn't really need to be private.
    result = nil
    stack = []

    tokens.each do |token|
      token = sanitize(token)
      case token
      when Float
        stack.push(token)
      when Symbol
        operator = token
        left_operand = stack.pop
        right_operand = stack.pop

        raise "calculator is empty" unless right_operand && left_operand

        result = right_operand.public_send(operator, left_operand)
        stack.push(result)
      else
        raise ArgumentError, "This token has caused an error: #{token.inspect}."
      end
    end
    result
  end
end
