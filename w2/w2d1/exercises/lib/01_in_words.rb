#Status: complete.

module InWords
  Small_numbers = ["zero", "one", "two", "three", "four", "five", "six",
    "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen",
    "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
  Tens = {2 => "twenty", 3 => "thirty", 4 => "forty", 5 => "fifty",
    6 => "sixty", 7 => "seventy", 8 => "eighty", 9 => "ninety"}
  Ten_to_n = {3 => "thousand", 6 => "million", 9 => "billion",
    12 => "trillion", 15 => "quadrillion", 18 => "quintillion",
    21 => "sextillion", 24 => "septillion", 27 => "octillion",
    30 => "nonillion", 33 => "decillion", 36 => "undecillion",
    39 => "duodecillion", 42 => "tredecillion", 45 => "quattuordecillion",
    48 => "quindecillion", 51 => "sexdecillion", 54 => "septendecillion",
    57 => "octodecillion", 60 => "novemdecillion", 63 => "vigintillion",
    66 => "unvigintillion", 69 => "duovigintillion", 72 => "trevigintillion",
    75 => "quattuorvigintillion", 78 => "quinvigintillion", 81 => "sexvigintillion",
    84 => "septenvigintillion", 87 => "octovigintillion", 90 => "novemvigintillion",
    93 => "trigintillion", 96 => "untrigintillion", 99 => "duotrigintillion"
  } #Number names taken from [https://www.unc.edu/~rowlett/units/large.html]

  def in_words
    return "zero" if self == 0
    remainder = self.abs

    hundreds, remainder = remainder % 1000, remainder / 1000
    words_in_reverse = (hundreds == 0) ? [] : [small_number_name(hundreds)]

    exponent = 3
    while remainder > 0
      raise "Number #{self} is too large." unless Ten_to_n.has_key?(exponent)
      prefix, remainder = remainder % 1000, remainder / 1000
      name = "#{small_number_name(prefix)} #{Ten_to_n[exponent]}"
      exponent += 3

      if prefix == 0 then next else words_in_reverse << name.strip end
    end

    words_in_reverse << "negative" if self < 0
    words_in_reverse.reverse.join(" ")
  end

  def small_number_name(number)
    raise ArgumentError unless number >= 0 && number < 1000
    return "zero" if number == 0

    remainder = number
    name = ""

    if remainder >= 100
      name << "#{Small_numbers[number / 100]} hundred"
      remainder = remainder % 100
    end

    if remainder >= 20
      name << " #{Tens[remainder / 10]}"
      remainder = remainder % 10
    end

    if remainder > 0
      name << " #{Small_numbers[remainder]}"
    end

    name.strip
  end
end

class Fixnum
  include InWords
end

class Bignum
  include InWords
end
