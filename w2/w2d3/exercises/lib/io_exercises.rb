#Status: complete.
#Status: ugly code.

def guessing_game(lower_bound=1, upper_bound=100)
  secret_number = lower_bound + rand(upper_bound - lower_bound + 1)
  attempts = 0
  guess = nil

  until guess == secret_number
    guess = nil
    prompt_message = "Please guess a number between #{lower_bound} and #{upper_bound}:  "

    guess = get_integer(prompt_message) while guess.nil?
    attempts += 1


    puts "It took you #{attempts} attempts." if correct_guess?(guess, secret_number)
    print "\n\n"
  end
end

def correct_guess?(guess, secret_number)
      case guess <=> secret_number
      when -1
        puts "#{guess} is too low."
      when 0
        puts "#{guess} is correct! Congratulations!"
        return true
      when 1
        puts "#{guess} is too high."
      end
      false
end

def get_integer(prompt_message)
  print prompt_message
  user_input = gets.chomp

  begin
    user_input = Integer user_input
  rescue ArgumentError, TypeError
    print "#{user_input} is not a valid guess. Please try again.\n"
    user_input = nil
  end
  user_input
end


if $PROGRAM_NAME == __FILE__
  guessing_game
end
