class Board
  def initialize(grid=nil)
    @grid = grid || Array.new(3) {Array.new(3, nil)}
    @width = @grid.count
    @height = @grid[0].count
    @player_ids = [:X, :O]
  end

  def grid
    @grid
  end

  def place_mark(coords, player)
    x = coords[0]
    y = coords[1]
    @grid[x][y] = player if @grid[x][y].nil?
  end

  def empty?(coords)
    x = coords[0]
    y = coords[1]
    @grid[x][y].nil?
  end

  def winner
    win_segments = []
    win_segments << columns << rows << diagonals
    @player_ids.each do |player|
      win_segments.each do |segments|
        return player if segments.any? do |segment|
          segment.all? {|mark| mark == player}
        end
      end
    end
    return nil
  end

  def columns
    cols = []
    0.upto(@width.pred  ) do |x|
      cols << @grid[x]
    end
    cols
  end

  def rows
    #Refactor note: it might be useful to define a transpose method and then simply call columns on the transposed array.
    _rows = []
    0.upto(@height.pred) do |y|
      row = []
      columns.each do |column|
        row << column[y]
      end
      _rows << row
    end
    _rows
  end

  def diagonals
    diags = [[], []]
    0.upto(@height.pred) do |coord|
      diags[0] << @grid[coord][coord]#bottom left to top right
      diags[1] << @grid[coord][@height.pred - coord]#top left to bottom right
    end
    diags
  end

  def over?
    return false if @grid==nil || @grid.empty?
    @grid.flatten.none? {|val| val.nil?} || winner
  end
end
