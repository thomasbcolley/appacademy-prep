#Status: complete.

class TowersOfHanoi

  attr_accessor :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    until won?
      render
      print "Please enter the number of the tower from which you wish to take a disc:  "
      from_tower = gets.chomp.to_i
      print "Please enter the number of the twoer to which you wish to place the disc:  "
      to_tower = gets.chomp.to_i
      unless valid_move?(from_tower, to_tower)
        puts "Invalid Move! Please try again!"
        next
      else
        move(from_tower, to_tower)
      end
    end
    render
    puts "You have won the game!"
  end

  def render
    print "-"*50 + "\n"
    print "tower 0:  #{towers[0].to_s}\n"
    print "tower 1:  #{towers[1].to_s}\n"
    print "tower 2:  #{towers[2].to_s}\n"
    print "-"*50 + "\n"
  end

  def won?
    @towers[0].empty? && (@towers[1].empty? || @towers[2].empty?)
  end

  def valid_move?(from_tower, to_tower)
    return false if @towers[from_tower].empty?
    if @towers[to_tower][-1].nil? || (@towers[from_tower][-1] < @towers[to_tower][-1])
      true
    else
      false
    end
  end

  def move(from_tower, to_tower)
    @towers[to_tower].push(@towers[from_tower].pop)
  end

end

if __FILE__ == $PROGRAM_NAME
  tower = TowersOfHanoi.new
  tower.play
end
