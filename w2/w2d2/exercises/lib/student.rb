=begin
I do not understand why the specs are failing. I have tried many things,
and speaking from a functional perspective, I have made the code do what
it is supposed to several different ways, yet the specs still fail with
vague error messages. I have done bundle install. I have done all that
has been asked of me. Yet the tests still fail. For now, I give up.
I will return some other time, but presently it simply isn't worth it.
STATUS: INCOMPLETE
=end


class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name, @last_name = first_name, last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    unless course.students.include?(self)
      @courses << course
      course.add_student(self)
    end
  end

  def course_load

  end
end
